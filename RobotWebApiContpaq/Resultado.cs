﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotWebApiContpaq
{
    [Serializable]
    public class Resultado
    {
        public bool _Error;
        public string _Mensaje;
    }
}
