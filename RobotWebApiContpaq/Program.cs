﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;
using System.Xml;

namespace RobotWebApiContpaq
{
    class Program
    {
        static string rutaXml = ConfigurationSettings.AppSettings["RutaXml"];
        static string rutaJson = ConfigurationSettings.AppSettings["RutaJson"];
        static string debug = ConfigurationSettings.AppSettings["Debug"];

        static string json;
        static HttpClient cliente = new HttpClient();
        static string url = "http://162.248.54.61:8081/api/Comercial";
        static string mensaje;
        static void Main(string[] args) 
       {
            string path =rutaJson+"json.json";
            Resultado res= new Resultado();

            using (StreamReader jsonStream = File.OpenText(path))
            {
                json = jsonStream.ReadToEnd();
            }//Obtener el Json desde la carpeta configurada

            try {
                var httpContent = new StringContent(json);
                httpContent.Headers.ContentType.MediaType = "application/json";
                
                HttpResponseMessage respuesta= cliente.PostAsync(url,httpContent).Result;
                respuesta.EnsureSuccessStatusCode();
                mensaje = respuesta.Content.ReadAsStringAsync().Result;
                res = (Resultado)JsonConvert.DeserializeObject(mensaje,typeof (Resultado));
            }
            catch (Exception e)
            {
                res._Error = true;
                res._Mensaje = e.Message;
                Console.WriteLine(e.Message);
            }//Hacer la solicitus a la Web API

            try
            {
                XmlDocument documentoxml = new XmlDocument();
                documentoxml.LoadXml(res._Mensaje);
                string folio = documentoxml.ChildNodes[1].Attributes[5].InnerText;//obtener el folio del documento
                string fecha = DateTime.Now.ToString("dd-MM-yyyy hh;mm;ss");

                documentoxml.Save(rutaXml + "Folio " + folio + "  " + fecha + ".xml");
            }
            catch (Exception e)
            {
                res._Error = true;
                res._Mensaje = e.Message;
                Console.WriteLine(e.Message);
            }//Guardar el xml de respuesta

            if (debug=="1")
            {
                Console.WriteLine("El archivo xml se guardo en: "+ rutaXml);
                Console.ReadKey();
            }

        }
        
    }
}
